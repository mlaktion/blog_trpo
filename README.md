# Blog

## Prerequisites

* Docker CE
* Docker Compose

## Build

```bash
docker-compose up build
```

(outputs jar in the `/target` dir)

## Run

First you need to bring up the database:
```bash
docker-compose up -d dev-db
```
It is up&running at localhost:5432 with creds:
username: `user`
pass: `password`

The database name is `blog_lab`. All the data is stored under `pgdata/` folder.

Now you can launch the app. You need to have only one built `.jar`. Follow steps in *Build* section. After that, run:

```bash
docker-compose up -d blog
```

The server is up and listening on localhost port 8080.

## Other

*Shutdown everything*
Run from project folder:
```bash
docker-compose down
```

*Cleanup the database*
Run from project folder:
```bash
util/cleanup_db.sh
```