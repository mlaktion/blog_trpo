pipeline {
    agent any
    environment {
        AZURE_CREDENTIALS = credentials('azure_portal_credentials')
        AZURE_TENANT = credentials('azure_tenant')
        AZURE_APP_NAME = 'trpo-blog'
        AZURE_RESOURCE_GROUP = 'TRPO'

        REGISTRY_CREDS = credentials('azure_cr_credentials')
        REGISTRY_URI = 'trpoc.azurecr.io'

        DOCKER_LOCAL_TAG = "trpo-blog:build-${BUILD_NUMBER}"
        DOCKER_REMOTE_TAG = "${REGISTRY_URI}/${REGISTRY_CREDS_USR}/trpo-blog:latest"

    }
    stages {
        stage('Test artifacts') {
            steps {
                sh 'docker-compose up test'
            }
        }
        stage('Build artifacts') {
            steps {
                sh 'docker-compose up build'
            }
        }
        stage('Build image') {
            steps {
                sh 'docker build --rm=true -t ${DOCKER_LOCAL_TAG} .'
            }
        }
        stage('Publish image') {
            steps {
                sh 'docker login -u ${REGISTRY_CREDS_USR} -p ${REGISTRY_CREDS_PSW} ${REGISTRY_URI}'
                sh 'docker tag ${DOCKER_LOCAL_TAG} ${DOCKER_REMOTE_TAG}'
                sh 'docker push ${DOCKER_REMOTE_TAG}'
            }
        }
        stage('Restart application') {
            steps {
                sh 'az login --service-principal --username ${AZURE_CREDENTIALS_USR} --password ${AZURE_CREDENTIALS_PSW} --tenant ${AZURE_TENANT}'
                sh 'az webapp restart --name ${AZURE_APP_NAME} --resource-group ${AZURE_RESOURCE_GROUP}'
            }
        }
    }
    post {
        success {
            archiveArtifacts artifacts: '**/target/*.jar', fingerprint: true
        }
        cleanup {
            sh 'docker image prune -f'
        }
    }
}
