#!/usr/bin/env sh

set -x

PATH_TO_JAR=$( find . -name 'blog-*.jar' )

java -jar ${PATH_TO_JAR}
