#!/usr/bin/env bash

set -x

docker-compose down
sudo rm -rf ./pgdata
docker-compose up -d dev-db