package ru.vsu.dto;

public class UserCreationResultResponseDTO extends ResponseDTO {
    private boolean operationResult;

    public UserCreationResultResponseDTO(String path, boolean operationResult) {
        super(path);
        this.operationResult = operationResult;
    }

    public boolean isOperationResult() {
        return operationResult;
    }

    @Override
    public String toString() {
        return "UserCreationResultResponseDTO{" +
                super.toString() +
                ", operationResult=" + operationResult +
                '}';
    }
}
