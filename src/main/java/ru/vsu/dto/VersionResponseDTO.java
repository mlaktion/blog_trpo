package ru.vsu.dto;

public class VersionResponseDTO extends ResponseDTO {
    private String version;

    public VersionResponseDTO(String path, String version) {
        super(path);
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    @Override
    public String toString() {
        return "VersionResponseDTO{" +
                super.toString() +
                ", version=" + version +
                '}';
    }
}
