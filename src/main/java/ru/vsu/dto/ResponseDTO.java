package ru.vsu.dto;

import java.time.LocalDateTime;

public abstract class ResponseDTO {
    private final String path;
    private final String currentTime;

    public ResponseDTO(String path) {
        this.path = path;
        this.currentTime = LocalDateTime.now().toString();
    }

    public String getPath() {
        return path;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    @Override
    public String toString() {
        return "ResponseDTO{" +
                "path='" + path + '\'' +
                ", currentTime='" + currentTime + '\'' +
                '}';
    }
}
