package ru.vsu.dto;

import java.util.List;

public class MessageListResponseDTO extends ResponseDTO {
    private List<MessageDTO> messages;

    public MessageListResponseDTO(String path, List<MessageDTO> messages) {
        super(path);
        this.messages = messages;
    }

    public List<MessageDTO> getMessages() {
        return messages;
    }

    @Override
    public String toString() {
        return "MessageListResponseDTO{" +
                super.toString() +
                ", messages=" + messages +
                '}';
    }
}
