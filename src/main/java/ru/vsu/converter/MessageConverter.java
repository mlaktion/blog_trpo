package ru.vsu.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vsu.dto.MessageDTO;
import ru.vsu.entity.Message;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageConverter {

    private UserConverter userConverter;

    public MessageDTO messageEntityToDTO(Message message) {
        MessageDTO messageDTO = new MessageDTO();
        if (message != null) {
            messageDTO.setId(message.getId());
            messageDTO.setTitle(message.getTitle());
            messageDTO.setText(message.getText());
            messageDTO.setTimeStamp(message.getTimeStamp());
            messageDTO.setUser(userConverter.userEntityToDTO(message.getUser()));
        }
        return messageDTO;
    }

    public Message messageDTOToEntity(MessageDTO messageDTO) {
        Message message = new Message();
        if (messageDTO != null) {
            message.setId(messageDTO.getId());
            message.setTitle(messageDTO.getTitle());
            message.setText(messageDTO.getText());
            message.setTimeStamp(messageDTO.getTimeStamp());
            message.setUser(userConverter.userDTOToEntity(messageDTO.getUser()));
        }
        return message;
    }

    public List<MessageDTO> messageListEntityToDTO(Iterable<Message> messages) {
        List<MessageDTO> messagesDTO = new ArrayList<>();

        for (Message msg : messages) {
            messagesDTO.add(messageEntityToDTO(msg));
        }

        return messagesDTO;
    }

    @Autowired
    public void setUserConverter(UserConverter userConverter) {
        this.userConverter = userConverter;
    }
}
