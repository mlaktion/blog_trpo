package ru.vsu.converter;

import org.springframework.stereotype.Service;
import ru.vsu.dto.UserDTO;
import ru.vsu.entity.User;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserConverter {

    public UserDTO userEntityToDTO(User user) {
        UserDTO userDTO = new UserDTO();
        if (user != null) {
            userDTO.setId(user.getId());
            userDTO.setLogin(user.getLogin());
            userDTO.setTimeStamp(user.getTimeStamp());
        }
        return userDTO;
    }

    public User userDTOToEntity(UserDTO userDTO) {
        User user = new User();
        if (userDTO != null) {
            user.setId(userDTO.getId());
            user.setLogin(userDTO.getLogin());
            user.setTimeStamp(userDTO.getTimeStamp());
        }
        return user;
    }

    public List<UserDTO> userListEntityToDTO(Iterable<User> users) {
        List<UserDTO> usersDTO = new ArrayList<>();

        for (User user : users) {
            usersDTO.add(userEntityToDTO(user));
        }

        return usersDTO;
    }
}
