package ru.vsu.repo;

import org.springframework.data.repository.CrudRepository;
import ru.vsu.entity.Message;

public interface MessageRepo extends CrudRepository<Message, Long> {
}