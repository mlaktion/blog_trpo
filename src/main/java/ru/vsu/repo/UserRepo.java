package ru.vsu.repo;

import org.springframework.data.repository.CrudRepository;
import ru.vsu.entity.User;


public interface UserRepo extends CrudRepository<User, Long> {
    User findByLogin(String login);
}