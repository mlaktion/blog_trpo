package ru.vsu.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;


@Entity
public class Message  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Size(min=1, max=75)
    @Column(name = "title")
    private String title;

    @Size(min=1, max=150)
    @Column(name = "text")
    private String text;

    @Column(name = "timestamp")
    private LocalDateTime timeStamp;

    @ManyToOne
    @JoinColumn(name = "user_id", updatable = false)
    private User user;

    public Message(String title, String text, LocalDateTime timeStamp, User user) {
        this.title = title;
        this.text = text;
        this.timeStamp = timeStamp;
        this.user = user;
    }

    public Message() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }
}
