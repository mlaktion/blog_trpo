package ru.vsu.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vsu.converter.MessageConverter;
import ru.vsu.converter.UserConverter;
import ru.vsu.dto.MessageDTO;
import ru.vsu.dto.UserDTO;
import ru.vsu.repo.MessageRepo;
import ru.vsu.repo.UserRepo;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class BlogService {
    private static final Logger LOG = LoggerFactory.getLogger(BlogService.class);
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private MessageRepo msgRepo;
    @Autowired
    private MessageConverter messageConverter;
    @Autowired
    private UserConverter userConverter;

    public List<UserDTO> findAllUsers() {
        try {
            return userConverter.userListEntityToDTO(userRepo.findAll());
        } catch (Exception e) {
            LOG.error("Failed to find all users", e);
        }
        return new ArrayList<>();
    }

    public List<MessageDTO> findAllMessages() {
        try {
            return messageConverter.messageListEntityToDTO(msgRepo.findAll());
        } catch (Exception e) {
            LOG.error("Failed to find all messages", e);
        }
        return new ArrayList<>();
    }

    public boolean createMessage(MessageDTO messageDTO) {
        try {
            messageDTO.setUser(userConverter.userEntityToDTO(userRepo.findByLogin("user")));
            messageDTO.setTimeStamp(LocalDateTime.now());
            msgRepo.save(messageConverter.messageDTOToEntity(messageDTO));
            return true;
        } catch (Exception e) {
            LOG.error("Failed to create a message", e);
        }
        return false;
    }

}
