package ru.vsu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.vsu.dto.MessageDTO;
import ru.vsu.dto.MessageListResponseDTO;
import ru.vsu.dto.ResponseDTO;
import ru.vsu.dto.UserCreationResultResponseDTO;
import ru.vsu.dto.VersionResponseDTO;
import ru.vsu.service.BlogService;

import java.util.concurrent.TimeUnit;

@RestController
public class MainController {
    @Autowired
    private BlogService blogService;

    @Value("${build.version}")
    private String buildVersion;

    @GetMapping("/version")
    public VersionResponseDTO main() {
        VersionResponseDTO response = new VersionResponseDTO("/version", buildVersion);
        return response;
    }

    @GetMapping("/allMessages")
    public MessageListResponseDTO goodEndpoint() {
        MessageListResponseDTO response = new MessageListResponseDTO("/allMessages", blogService.findAllMessages());
        return response;
    }

    @GetMapping("/exception")
    public ResponseDTO throwsException() {
        throw new RuntimeException();
    }

    @PostMapping("/createWithDelay")
    public UserCreationResultResponseDTO delay(MessageDTO messageDTO) {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        UserCreationResultResponseDTO response = new UserCreationResultResponseDTO("/delay", blogService.createMessage(messageDTO));
        return response;
    }

}
