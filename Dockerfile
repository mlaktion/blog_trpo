FROM openjdk:8u191-jdk-alpine3.9

RUN mkdir /app
WORKDIR /app

COPY ./target/*.jar ./
COPY ./util/run.sh ./

EXPOSE 8080

CMD ./run.sh
